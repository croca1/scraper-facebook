import json
import numpy as np
import requests
import re
import http.client

from facebook_scraper import get_posts
from flask import Flask, request, Response
from flask_cors import CORS

checkpoint_file = 'checkpoint.json'
posts_file = 'posts.json'

def saveCheckoint(post_id):
    checkpoint = {
        'recent_post_id': post_id
    }
    with open(checkpoint_file, 'w') as outfile:
        json.dump(checkpoint, outfile)

def readCheckpoint():
    try:
        with open(checkpoint_file) as json_file:
            data = json.load(json_file)
            return data
    except:
        return None
    
def removeDupes(mylist):
    newlist = [mylist[0]]
    for e in mylist:
        if e not in newlist:
            newlist.append(e)
    return newlist

def saveData(posts, profile_id):
    old_posts = []
    file_name = 'posts_' + profile_id + '.json'
    try:
        with open(file_name) as json_file:
            old_posts = json.load(json_file)
    except:
        print('post file not created!')

    with open(file_name, 'w') as outfile:
        json.dump(removeDupes(old_posts + posts), outfile)

def getPosts(profile_id):
    posts = []
    first_post_id = None
    if readCheckpoint() is None:
        harvest_cut_post_id = None
    else:
        harvest_cut_post_id = readCheckpoint()['recent_post_id']
    for post in get_posts(profile_id, pages=5):
        if harvest_cut_post_id == post['post_id']:
            if first_post_id is None:
                first_post_id = harvest_cut_post_id
            break
        else:
            posts.append({
                'id': post['post_id'],
                'text': post['text'],
                'time': post['time'].__str__(),
                'post_url': post['post_url']
            })
            if first_post_id is None:
                first_post_id = post['post_id']

    saveCheckoint(first_post_id)
    saveData(posts, profile_id)

def sentimeAnalysisService(profile_id):
    post_r = []
    file_name = 'posts_' + profile_id + '.json'
    with open(file_name) as json_file:
        posts = json.load(json_file)
    conn = http.client.HTTPSConnection("bsttmqa1ja.execute-api.us-east-1.amazonaws.com")
    for post in posts:
        text = post["text"]
        payload = "{\n\t\"text\": \"" + text + "\"\n}"
        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache",
        }
        conn.request("POST", "/dev/candidates", payload.encode('utf-8'), headers)
        res = conn.getresponse()
        res = json.loads(res.read())
        try:
            post_r.append({
                'fb_id': post['id'],
                'text': post["text"],
                'sentimentAnalysis': res['sentimentAnalysis']['Sentiment'],
                'keyPhrases': res['keyPhrases']['KeyPhrases'],
                'post_url': post["post_url"]
            })
        except:
            post_r.append({
                'fb_id': post['id'],
                'text': post["text"],
                'post_url': post["post_url"]
            })
    return post_r


app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/sentiment_analysis/<profile_id>', methods=['POST'])
def getPostWithAnalysis(profile_id):
    print(profile_id)
    getPosts(profile_id)
    results = sentimeAnalysisService(profile_id)
    return {
        "estado": True,
        "resultado": results
    }


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

