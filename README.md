# Facebook scraper para analisis de sentimiento

El proyecto se desarrollo usando python 3.9

### 1. Configuración
```sh
$ python -m venv SF
$ source SF/bin/activate
$ pip install -r requirements.txt
$ python run_scraper.py
```